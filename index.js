const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// useNew and useUnified- used to avoid confusion with mondogb
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.55eun.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true

})

// Set notifications for connection success or failure
// Connection to the database
let db = mongoose.connection;

// if may error sa connection, output a message in the console.
// console.error.bind - to print error in console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output a message in the console
db.once("open", ()=> console.log("We're connected to the cloud database"));

// Create a Task schema
const taskSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		// default values are the predefined values for a field
		default: "pending"
	}
})

// User Schema
	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	})

// Creating Models
// Server > Schema > Database > Collection(MongoDb)
// Task- maccreate na collection
const Task = mongoose.model("Task", taskSchema);

// User Model
const User =mongoose.model("User", userSchema);


app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req,res) => {
	Task.findOne({name : req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found") 
		}
		// If no document found
		else{
			// Create a new task and save itto the database
			let newTask = new Task(
			{
				name : req.body.name
			})
			// .save is used to store info in the database
			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err)
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// Create a POST route that will access the "/signup" route that will create a user.
app.post("/signup", (req, res) => {
	User.findOne({username : req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found")
		}
		else{
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user created");
				}
			})
		}
	})
})

// Retrieve all data
app.get("/signup", (req,res)=>{
	User.find({}, (err, result)=> {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`))